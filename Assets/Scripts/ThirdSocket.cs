using UnityEngine;

public class ThirdSocket : MonoBehaviour
{
    private ThirdPuzzleManager thirdpuzzleManager;
    private bool isFilled = false;

    void Start()
    {
        thirdpuzzleManager = FindObjectOfType<ThirdPuzzleManager>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Object") && !isFilled) // Ensure your puzzle objects have this tag
        {
            isFilled = true;
            thirdpuzzleManager.OnObjectPlaced();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Object") && isFilled)
        {
            isFilled = false;
            thirdpuzzleManager.OnObjectRemoved();
        }
    }

    
}

