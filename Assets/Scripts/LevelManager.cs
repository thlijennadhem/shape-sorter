using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public int requiredObjects = 4; // Adjust the number based on your game

    // Update is called once per frame
    void FixedUpdate()
    {
        if (CheckLevelCompletion())
        {
            LoadNextLevel();
        }
    }

    bool CheckLevelCompletion()
    {
        // Implement logic to check if the level is complete
        // For example, check if the required number of objects are in their sockets
        int objectsInSockets = CountObjectsInSockets();
        return objectsInSockets >= requiredObjects;
    }

    int CountObjectsInSockets()
    {
        // Implement logic to count the number of objects in sockets
        // You may need to reference the sockets and objects in your scene
        // and check if they are in the correct positions.
        // Return the count.
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Object");
        int count = 0;


        foreach (var obj in objects)
        {
            // Replace "Socket" with the tag or name you use for your sockets
            if (obj.CompareTag("Socket"))
            {
                count++;
            }
            //Debug.Log(count);
        }

        return count;
    }

    void LoadNextLevel()
    {
        // Load the next scene based on the current scene index
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        
        // Check if there is a next scene available
        if (currentSceneIndex + 1 < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(currentSceneIndex + 1);
        }
        else
        {
            // If there is no next scene, consider restarting the game or showing a game completion screen
            Debug.LogWarning("No next scene available. Consider restarting the game or showing a completion screen.");
        }
    }
}
