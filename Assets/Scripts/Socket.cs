using UnityEngine;

public class Socket : MonoBehaviour
{
    private PuzzleManager puzzleManager;
    private bool isFilled = false;

    void Start()
    {
        puzzleManager = FindObjectOfType<PuzzleManager>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Object") && !isFilled) // Ensure your puzzle objects have this tag
        {
            isFilled = true;
            puzzleManager.OnObjectPlaced();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Object") && isFilled)
        {
            isFilled = false;
            puzzleManager.OnObjectRemoved();
        }
    }

    
}

