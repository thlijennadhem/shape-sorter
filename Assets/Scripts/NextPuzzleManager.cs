using UnityEngine;
using UnityEngine.SceneManagement;

public class NextPuzzleManager : MonoBehaviour
{
    private int filledSocketsCount = 0;
    private int totalSockets = 4;

    // Call this method when an object is placed in a socket
    public void OnObjectPlaced()
    {
        filledSocketsCount++;
        Debug.Log("Object placed. Filled sockets count: " + filledSocketsCount);
        CheckAllSocketsFilled();
    }

    public void OnObjectRemoved()
    {
        filledSocketsCount--;
        Debug.Log("Object removed. Filled sockets count: " + filledSocketsCount);
    }
    

    // Check if all sockets are filled
    private void CheckAllSocketsFilled()
    {
        if (filledSocketsCount >= totalSockets)
        {
            Debug.Log("All sockets filled. Loading next scene...");
            LoadNextScene();
        }
    }

    // Load the next scene
    private void LoadNextScene()
    {
        // Replace "NextScene" with the name of your next scene
        SceneManager.LoadScene("Level3");
    }
}
