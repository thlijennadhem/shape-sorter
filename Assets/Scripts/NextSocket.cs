using UnityEngine;

public class NextSocket : MonoBehaviour
{
    private NextPuzzleManager nextpuzzleManager;
    private bool isFilled = false;

    void Start()
    {
        nextpuzzleManager = FindObjectOfType<NextPuzzleManager>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Object") && !isFilled) // Ensure your puzzle objects have this tag
        {
            isFilled = true;
            nextpuzzleManager.OnObjectPlaced();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Object") && isFilled)
        {
            isFilled = false;
            nextpuzzleManager.OnObjectRemoved();
        }
    }

    
}

